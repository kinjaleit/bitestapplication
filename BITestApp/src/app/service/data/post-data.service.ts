import { API_URL } from './../../app.constants';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from '../../homepage/homepage.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostDataService {

  constructor(
    private http:HttpClient
  ) { }
 //Passed username if login needed for reference but its not required 

  retrieveAllPosts(username) {
    console.log(`${API_URL}`)
    return this.http.get<Post[]>(`${API_URL}/posts`);
  }

  deletePost(username, id){
    return this.http.delete(`${API_URL}/posts/${id}`);
  }

  retrievePost(username, id){
    return this.http.get<Post>(`${API_URL}/posts/${id}`);
  }

  retrievePostByUserId(username, userId){
    return this.http.get<Post[]>(`${API_URL}/posts?userId=${userId}`);
  }

  updatePost(username, id, post){
    return this.http.put(`${API_URL}/posts/${id}`, post);
  }

  createPost(username, post){
    return this.http.post(`${API_URL}/posts`, post);
  }

}

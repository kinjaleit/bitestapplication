import { Component, OnInit } from '@angular/core';
import { PostDataService } from 'src/app/service/data/post-data.service';
import { ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Post } from '../../homepage/homepage.component';
import errorHandler from 'src/app/errorHandler';

@Component({
  selector: 'app-postdetails',
  templateUrl: './postdetails.component.html',
  styleUrls: ['./postdetails.component.css']
})
export class PostdetailsComponent implements OnInit {

  id:number
  post: Post
  isShown:Boolean = true;

  constructor(private postService: PostDataService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.post = new Post('',this.id,'','');
    this.id = this.route.snapshot.params['id']; 
      console.log(this.id);
      this.postService.retrievePost('user', this.id)
          .subscribe (
            data => this.post = data
          ,errorHandler)
  }

  toggleShow(){
    this.isShown = ! this.isShown;
  }

}

import { Component, OnInit } from '@angular/core';
import { Post } from '../../homepage/homepage.component';
import { ActivatedRoute, Router } from '@angular/router';
import { PostDataService } from 'src/app/service/data/post-data.service';
import errorHandler from 'src/app/errorHandler';

@Component({
  selector: 'app-createupdatepost',
  templateUrl: './createupdatepost.component.html',
  styleUrls: ['./createupdatepost.component.css']
})
export class CreateupdatepostComponent implements OnInit {

  id:number
  post: Post

  constructor(
    private postService: PostDataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    
    this.id = this.route.snapshot.params['id'];
    
    this.post = new Post('',this.id,'','');
    //if -1 we will be adding Post and not -1 then updating the post
    if(this.id!=-1) {
      this.postService.retrievePost('user', this.id)
          .subscribe (
            data => this.post = data
            ,errorHandler);
    }
  }

  savePost() {
    if(this.id == -1) { //create post
      this.postService.createPost('user', this.post)
          .subscribe (
            data => {
              console.log(data)
              this.router.navigate(['homepage',"user"])
              
               alert(`Add of Post Successful!`);   
            },errorHandler);
    } else { //update post
      this.postService.updatePost('user', this.id, this.post)
          .subscribe (
            data => {
              console.log(data)
              this.router.navigate(['homepage',"user"])
              alert(`Update of Post ${this.id} Successful!`);   
            },errorHandler);
    }
  }

}

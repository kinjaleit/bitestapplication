import { HardcodedAuthenticationService } from './../service/hardcoded-authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = 'user'
  password = ''
  errorMessage = 'Invalid Credentials'
  invalidLogin = false

  constructor(private router: Router,
    private hardcodedAuthenticationService: HardcodedAuthenticationService) { }

  ngOnInit() {
  }

  handleLogin() {
    // console.log(this.username);
    if(this.hardcodedAuthenticationService.authenticate(this.username, this.password)) {
      //Redirect to home Page with harcorded authentication but in future we can implement JWT authentication by creating HTTP_INTERCEPTOR service
      this.router.navigate(['homepage', this.username])
      this.invalidLogin = false
    } else {
      this.invalidLogin = true
    }
  }

}

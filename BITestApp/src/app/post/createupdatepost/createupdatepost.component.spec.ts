import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateupdatepostComponent } from './createupdatepost.component';

describe('CreateupdatepostComponent', () => {
  let component: CreateupdatepostComponent;
  let fixture: ComponentFixture<CreateupdatepostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateupdatepostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateupdatepostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

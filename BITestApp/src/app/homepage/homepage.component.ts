import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostDataService } from '../service/data/post-data.service';
import errorHandler from '../errorHandler';

export class Post {
  constructor(
    public userId: string,
    public id: number,
    public title: string,
    public body: string
  ){

  }
}

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  public  posts: Post[]
  filtereduserids:Post[]
  message: string
  distinctUserIds:any[];
  selectedvalue:string

  
  constructor(
    private postService:PostDataService,
    private router : Router
  ) { }

  ngOnInit() {
    this.refreshPosts();   
  }

  refreshPosts(){
    this.postService.retrieveAllPosts('user').subscribe(
      response => {
        console.log(response);
        this.posts = response;
        this.filtereduserids = response;
       this.getfilteruserids();
      },errorHandler);
  }

  //filter response of all retrived posts with distinct userid
  getfilteruserids(){    

     this.distinctUserIds =  this.filtereduserids.map(item => item.userId)
     .filter((value, index, self) => self.indexOf(value) === index);
     this.distinctUserIds.unshift("");
  }

  //Deleting the post
  deletePost(id) {
    console.log(`delete post ${id}` )
    this.postService.deletePost('user', id).subscribe (
      response => {
        console.log(response);
        this.message = `Delete of Post ${id} Successful!`;       
        this.refreshPosts();
      },errorHandler);
  }

  //navigate to update post screen
  updatePost(id) {
    console.log(`update ${id}`)
    this.router.navigate(['posts',id])
  }

  //navigate to post details screen
  selectPost(id) {
    console.log(`Select ${id}`)
    this.router.navigate(['post',id])
  }

  //navigate to create post screen
  addPost() {
    this.router.navigate(['posts',-1])  
  }

  //filter table with userid
  datachanged(e){
    if(e === ""){
      this.refreshPosts();
    }else{
        this.selectedvalue=e;
        this.postService.retrievePostByUserId('userById',this.selectedvalue).subscribe(
          response => {
            this.posts = response;
          },errorHandler);
      } 
  }
}

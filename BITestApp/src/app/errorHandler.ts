import {HttpErrorResponse} from '@angular/common/http';

const errorHandler = (err:HttpErrorResponse):any => {
    if(err.error instanceof Error){
        alert('Client Side Error');
    }
    else{
        alert('Server Side Error');
    }
};

export default errorHandler;
